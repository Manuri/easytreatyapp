This is the demo application for Easy Treaty Location based disease treatment project for GSoC 2014.

The app is hosted here.

https://googledrive.com/host/0B6Zv4ImMrSrkcWNPSGRmOVpVZjg/index.html

You can either go to above link or download the app and run it in local host.

Or inside packages/android you can find the .apk that can be run on an android phone of API level 17
.

Here are the screenshots of the newest version.

https://drive.google.com/folderview?id=0B6Zv4ImMrSrkeHhTeUpucDdCNWc&usp=sharing

=======================Guidelines to use the demo app==============================


-> When you open the app you
 will be given 3 choices.

1. To search medical centers (these data are taken through OpenMRS REST API)
2. To search doctors(these data is taken from a local file)
3. To search pharmacies(these data is taken from a local file)


-> After choosing one, you will be directed to the MapView which shows results on the Map.When you click on a marker you will get an InfoWindow with some details about the place and two buttons "Get Directions" and "More Details".

If you click "Get Directions" you will be given the directions to go to that place or if direction service is not available for that place a message window will be shown to tell that.

If you click on "More details" you will be directed to a new view which shows more details about the place.

In the Map View on the top toolbar you can see a "more" icon at the left. When you click that a sliding menu will be opened.

In that menu you will see the options,

1. Add Filters - If you click this you will be directed to the filterView and you can add filters. Currently filtering happens only according to the distance value you enter.

2. Sort - If you click this you will be directed to the ListView where you can see the List of results and you can sort according to the distance. (more options will be added to the sort later)

3. Change Location - If you click this you will be shown a message box which says what to do. Then after selecting your preferred location and press "Done" your search and filtering will again happen relative to the new location you chose.

4. Traffic Layer on - If you click this traffic layer will be shown on the Map. But google doesnt provide this for every country

5. Log In -  If you click this you will be directed to the Log In View where you can log in. Actual log in functionality is not implemented yet. But if you click the log in button in this LogIn View you will be directed back to MapView and a new option will appear on the Menu "Show Health Profile". By clicking this you will be directed to Profile View. And Log In button will have changed to Log out.

6. About -  If you click this you will see a message box showing details about the app.


-> In the bottom tool bar of the MapView you will see buttons "Medical Centers", "Pharmacies" and "Doctors". By clicking these you switch your search.



