﻿Ext.define('EasyTreatyApp.view.TestView', {
    extend: 'Ext.Container',

    xtype: 'testview',

    config: {
        layout: 'card',
        //layout:'fit',

        items: [{
            xtype: 'mappanel'
        },

                //{
                //    xtype: 'toolbar',
                //    docked: 'bottom',
                //    items: [

                //        {
                //            xtype: 'textfield',
                //            label: 'Disease',
                //            name: 'disease',
                //            width:'25%'
                //        },
                //        {
                //            xtype: 'textfield',
                //            label: 'Max Cost',
                //            name: 'maxcost',
                //            width: '25%'
                //        },
                //        {
                //            xtype: 'textfield',
                //            label: 'Min Cost',
                //            name: 'mincost',
                //            width: '25%'
                //        },
                //        {
                //            xtype: 'textfield',
                //            label: 'Max Distance',
                //            name: 'distance',
                //            width: '25%'
                //        }
                //    ]
                //},
                {
                    xtype: 'toolbar',
                    docked: 'top',
                    items: [
                        {
                            xtype: 'button',
                            text: 'Mark'
                        },
                        {
                            xtype: 'button',
                            text: 'Back',
                            docked: 'right'
                        },
                        {
                            xtype: 'button',
                            text: 'Add Filters'
                        }
                    ]
                }
        ]
    }

})