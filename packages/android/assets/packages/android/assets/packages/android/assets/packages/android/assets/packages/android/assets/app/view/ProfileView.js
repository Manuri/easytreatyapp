﻿Ext.define('EasyTreatyApp.view.ProfileView', {
    extend: 'Ext.Container',
    xtype: 'profileview',
    
    config: {
        cls:'profile-view',
        items: [
            {
                xtype:'userprofile'
            },
            {
                xtype: 'toolbar',
                docked: 'bottom',
                items: [
                    {
                        xtype: 'button',
                        text: 'Back',
                        docked: 'right'
                    }
                ]
            }
        ]
    }
})